import React, {Component} from "react"

export default class Item extends Component{
    render(){
        const {descricao, valor} = this.props;
        return(
        <div className = "produtos">
          <div className = "Geral">
              <ul className = "ul">
                  <input className="check" type="checkbox"/>
                  <li className = "lista">
                      Nome: {descricao}
                      <br/>
                      Valor: R${valor}
                  </li>
              </ul>
          </div>
        </div>
    )}
}
