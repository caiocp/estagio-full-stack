import React, { Component } from 'react';
import './App.css';
import Item from './Item.js';

class App extends Component {

  state = {
    isLoaded: false,
    users: []
  }

  componentDidMount() {

    fetch('http://api.jsonbin.io/b/5c63035e1198012fc895fba7')
      .then(response => response.json())
      .then(data =>{
        this.setState({
          users: data,
          isLoaded: true
        })
      })
  }

  render() {
    const { isLoaded, users } = this.state
    return (
      <div>
        <div className="App-header">
          <div className="Title">
            <h1 className = "Titulo">Titulo da Pagina</h1>
          </div>
          <h3 className = "Titulo_Tabela">Titulo da tabela</h3>
          <div className = "scrolavel">
            { isLoaded ? (
              users.map(user => {
                const{ descricao, valor } = user;
                return( <Item descricao={descricao} valor={valor} /> );
              })
            ) : (
              <h1 className = "Carregando">Carregando</h1>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
