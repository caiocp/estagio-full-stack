import React from 'react'
import './App.css'

export default class Carrinho extends React.Component {

  render(){
    const {lista} = this.props;

    return(
      <div>
        <h3 className="tProduto">Produto</h3>
        <h3 className="tValor">Valor</h3>
        <div className="Carrinho">
          {lista.map(item => {
            return(
              <li type="none">
                <div className="pNome">
                  {item.descricao}
                </div>
                <div className="pValor">
                  {item.valor}
                </div>
              </li>
            )
          })}
        </div>
      </div>
    )
  }

}
