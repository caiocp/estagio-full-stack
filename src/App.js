import React from 'react'

import Item from './Item'
import Carrinho from './Carrinho'
import './App.css'

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      dados: [],
      isLoaded: false,
      noCarrinho: [],
      valorTotal: 0
    }
  }

  componentDidMount() {
    fetch('http://api.jsonbin.io/b/5c63035e1198012fc895fba7')
    .then(res => res.json())
    .then(json => {
      this.setState({
      isLoaded: true,
      dados: json,
      })
    });
  }

  toggleApp = (obj, existe, valor) => {
    var {valorTotal} = this.state
    let updateCarrinho = this.state.noCarrinho;
    let novoValor = this.state.valorTotal;
    if (existe){
      updateCarrinho = [...updateCarrinho, obj]
      novoValor = Math.round((valorTotal + valor) * 100) /100
      console.log(novoValor)
    }
    else{
      console.log("objeto:", obj.id)
      updateCarrinho = [...updateCarrinho.filter(item => item.id !== obj.id)]
      novoValor = Math.round((valorTotal - valor) * 100) /100
      console.log(novoValor)
      updateCarrinho.map(nome => console.log("carrinho:", nome.id, "nome:",nome.descricao))
    }
    this.setState({
      noCarrinho: updateCarrinho,
      valorTotal: novoValor
    })
  }

  render() {
    const {isLoaded, dados, valorTotal} = this.state;
    return(
      <div className= "App">
        <div>
          <h5 className="titulo">
            Caio Carvalho Portela <br/>
            ccarvalho451@gmail.com
          </h5>
        </div>
        <h3 className="produtoTitulo">Lista de Produtos</h3>
        <div className="itens">
          <div className="produtos">
            {isLoaded ? (
              dados.map(dados =><Item toggleApp={this.toggleApp} {...dados} />)
            ) : (<h2 className="carregando">Carregando...</h2>)}
          </div>
          <h2 className="tCarrinho">Carrinho</h2>
          <Carrinho lista={this.state.noCarrinho} soma={this.state.valorTotal} />
        </div>
        <div>
          <h3 className="total">Valor Total:</h3>
          <h3 className="soma">R${valorTotal}</h3>
        </div>
      </div>
    )
  }
}
