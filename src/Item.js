import React from 'react'
import './App.css'

export default class Item extends React.Component {

  constructor() {
    super();
    this.state = {
      existe: true
    }
  }

  toggleChange = (obj) => {
    this.setState({
      existe: !this.state.existe
    })
    var {valor} = this.props
    valor = parseFloat(valor)
    this.props.toggleApp(obj, this.state.existe, valor)
  }

  render() {
    const {descricao, codigo, valor, id} = this.props;

      return(
        <div>
          <ul type="none">
              <li>
                <div>
                  <input type="checkbox" onClick={() => {this.toggleChange({"descricao":descricao, "valor":valor, "id":id})}}  className="check"/>
                  Produto: {descricao} | Código: {codigo}<br/>
                  Valor: R${valor}
                  <br/><br/>
                </div>
              </li>
          </ul>
        </div>
      )
    }
  }
